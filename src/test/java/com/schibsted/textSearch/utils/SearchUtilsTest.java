package com.schibsted.textSearch.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;

import com.schibsted.textSearch.model.KeyValueForFiles;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchUtilsTest {

    SearchUtils utils = new SearchUtils();

    @Test
    public void shouldMatch() {
        HashSet<String> set = new HashSet<String>();
        set.add("shikhar");
        set.add("stockholm");
        String input = "shikhar stockholm";
        assertTrue(utils.match(set, input.split(" ")) == 2);
    }

    @Test
    public void shouldCompute() {
        int a = 10;
        int b = 30;
        assertEquals(utils.compute(a, b), "33.33");
    }

    @Test
    public void shouldLoadFiles() {
        String pathToDir = "src/test/resources";
        Path path = Paths.get(pathToDir);
        List<KeyValueForFiles> list = utils.load(path);
        assertTrue(list.size() == 2);
        list.forEach(keyValueForFiles -> {
            if(keyValueForFiles.getKey().equalsIgnoreCase(pathToDir+"/One Text.txt")) {
                assertTrue(keyValueForFiles.getValue().size()==14);
                assertTrue(keyValueForFiles.getValue().contains("shikhar"));
            }
            if(keyValueForFiles.getKey().equalsIgnoreCase(pathToDir+"/Two Text.txt")) {
                assertTrue(keyValueForFiles.getValue().size() == 15);
                assertTrue(keyValueForFiles.getValue().contains("olivia"));

            }
        });
    }
}

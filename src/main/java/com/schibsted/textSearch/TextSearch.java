package com.schibsted.textSearch;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import com.schibsted.textSearch.model.KeyValueForFiles;
import com.schibsted.textSearch.utils.SearchUtils;

public class TextSearch {
    private static SearchUtils utils = new SearchUtils();

    public static void main(String[] args) {

        if (args.length == 0) {
            throw new IllegalArgumentException("Directory to Index mandatory");
        }
        Path dir = Paths.get(args[0]);
        List<KeyValueForFiles> listOfHashSets = utils.load(dir);
        System.out.println("Total files read: "+ listOfHashSets.size());
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("search> ");
            String input = sc.nextLine();
            if (input.equalsIgnoreCase("exit")) {
                System.exit(0);
            }
            String[] split = input.split(" ");
            for (KeyValueForFiles m : listOfHashSets) {
                int matched = utils.match(m.getValue(), split);
                String compute =utils.compute(matched, split.length);
                System.out.println("matched percentage for filename: "+m.getKey()+" is "+compute);
            }
        }

    }

}

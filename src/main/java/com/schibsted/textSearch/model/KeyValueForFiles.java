package com.schibsted.textSearch.model;

import java.util.HashSet;

public class KeyValueForFiles {
    private HashSet<String> value;
    private String key;

    public KeyValueForFiles(String key, HashSet<String> value) {
        this.value = value;
        this.key = key;
    }

    public HashSet<String> getValue() {
        return value;
    }

    public void setValue(HashSet<String> value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

package com.schibsted.textSearch.utils;

import java.io.File;
import java.math.RoundingMode;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.schibsted.textSearch.model.KeyValueForFiles;

public class SearchUtils {

    public int match(HashSet<String> file, String[] strings) {
        int contains = 0;
        for (String s : strings) {
            if (file.contains(s)) {
                contains++;
            }
        }
        return contains;
    }

    public String compute(int matching, int total) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_UP);
        Double a = matching*(100.00);
        Double b = a/total;
        return df.format(b);
    }

    public List<KeyValueForFiles> load(Path dir) {
        List<KeyValueForFiles> listOfHashSets = new ArrayList<>();
        try (DirectoryStream directoryStream = Files.newDirectoryStream(dir)) {
            Iterator i = directoryStream.iterator();
            while (i.hasNext()) {
                String fileName = i.next().toString();
                File f = new File(fileName);
                Scanner scan = new Scanner(f);
                scan.useDelimiter(" ");
                HashSet<String> stringHashSet = new HashSet<>();
                while (scan.hasNext()) {
                    String trimmed = scan.next().toLowerCase().replaceAll("[,.;:]", "");
                    stringHashSet.add(trimmed);
                }
                listOfHashSets.add(new KeyValueForFiles(fileName, stringHashSet));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listOfHashSets;
    }
}

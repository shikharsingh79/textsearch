**Text Search Function**

This is a project to enable searching for words in memory, after loading them from files.

The project can be built using maven, pom file is included. An executable jar file can be created
by running mvn clean package. 

The jar file can be executed in the following manner:


java -jar textSearch-1.0-SNAPSHOT-jar-with-dependencies.jar $directory

where $directory is the path to the directory containing all the files to uploaded to memory. 

The program will start and provide a prompt like this:

Total files read: 2 \
search> \
__

You may enter words to search after the prompt. The results will be displayed after you enter all the words and press 
enter. The program will consider all characters divided by a space character as a single word. So, for example, input:

you and I are going to Stockholm

will be considered 7 words to be searched. 

The percentage of matching is simply the number of words matching, divided by total number of words in input times 100. 

To exit the program, please enter "exit".


